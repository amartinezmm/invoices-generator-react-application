require("dotenv").config()
require("./mongo")

const express = require("express")
const app = express()
const cors = require("cors")

const notFound = require("./middleware/notFound.js")
const handleErrors = require("./middleware/handleErrors.js")

const usersRouter = require("./controllers/users")
const loginRouter = require("./controllers/login")
const invoicesRouter = require("./controllers/invoices")

app.use(cors())
app.use(express.json())

app.use(express.static("client/build"))

app.use("/api/users", usersRouter)
app.use("/api/login", loginRouter)
app.use("/api/invoices", invoicesRouter)

app.use(notFound)

// app.use(Sentry.Handlers.errorHandler())
app.use(handleErrors)

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/build/index.html"))
})

const PORT = process.env.PORT || 3001
const server = app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})

module.exports = { app, server }
