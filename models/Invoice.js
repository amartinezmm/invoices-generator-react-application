const { Schema, model } = require("mongoose")

const invoiceSchema = new Schema({
  invoice_id: String,
  date: Date,
  // TODO client_id reference with client document _id
  products: [
    { name: String, number_pkgs: Number, quantity: Number, unit_price: Number },
  ],
  tax: Number,
  discount: Number,
  total: Number,
  currency: String,
  payment: [{ method: String, information: String }],
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
})
invoiceSchema.set("toJSON", {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id
    returnedObject.products.map((item, key) => {
      item.id = item._id
      delete item._id
      return item
    })
    delete returnedObject._id
    delete returnedObject.__v
  },
})

const Invoice = model("Invoice", invoiceSchema) // creating the model from the schema

module.exports = Invoice // exporting the model
