const invoicesRouter = require("express").Router()
const Invoice = require("../models/Invoice")
const userExtractor = require("../middleware/userExtractor")
const User = require("../models/User")

invoicesRouter.get("/", userExtractor, async (request, response, next) => {
  console.log("hello rquest")
  const invoices = await Invoice.find({}).populate("user", {
    username: 1,
    invoices: 1,
  })

  console.log(invoices)
  response.json(invoices)
})

invoicesRouter.get("/:id", userExtractor, (request, response, next) => {
  const { id } = request.params

  Invoice.findById(id)
    .then((invoice) => {
      if (invoice) return response.json(invoice)
      response.status(404).end()
    })
    .catch((err) => next(err))
})

invoicesRouter.put("/:id", userExtractor, (request, response, next) => {
  console.log(request.body)
  const { id } = request.params
  const {
    invoice_id,
    date,
    products,
    tax,
    discount,
    total,
    currency,
    payment,
  } = request.body

  const newInvoiceInfo = {
    invoice_id: invoice_id,
    date: date,
    products: products,
    tax: tax,
    discount,
    discount,
    total: total,
    currency: currency,
    payment: payment,
  }

  Invoice.findByIdAndUpdate(id, newInvoiceInfo, { new: true })
    .then((result) => {
      response.json(result)
    })
    .catch(next)
})

invoicesRouter.post("/", userExtractor, async (request, response, next) => {
  const {
    invoice_id,
    date,
    products,
    tax,
    discount,
    total,
    currency,
    payment,
  } = request.body

  const { userId } = request

  const user = await User.findById(userId)

  if (!invoice_id) {
    return response.status(400).json({
      error: "required invoice_id field is missing",
    })
  }

  const newInvoice = new Invoice({
    invoice_id,
    date,
    products,
    tax,
    discount,
    total,
    currency,
    payment,
    user: user.id,
  })

  try {
    const savedInvoice = await newInvoice.save()

    user.invoices = user.invoices.concat(savedInvoice._id)
    await user.save()

    response.json(savedInvoice)
  } catch (error) {
    next(error)
  }
})

invoicesRouter.delete(
  "/:id",
  userExtractor,
  async (request, response, next) => {
    try {
      const invoice = await Invoice.findByIdAndRemove(request.params.id)
      return response.send("Item removed successfully")
    } catch (err) {
      next({ status: 400, message: "failed to remove the invoice" })
    }
  }
)

module.exports = invoicesRouter
