import { useEffect, useState } from "react"
import getInvoices from "services/invoices/getInvoices"
export default function useInvoices() {
  const [loading, setLoading] = useState(false)
  const [invoices, setInvoices] = useState([])

  useEffect(function () {
    console.log("hello")
    setLoading(true)
    console.log(
      getInvoices().then((invoices) => {
        setInvoices(invoices)
        setLoading(false)
      })
    )
  }, [])

  return { loading, invoices, setInvoices }
}
