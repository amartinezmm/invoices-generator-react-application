import { useEffect, useState } from "react"
import getInvoiceById from "services/invoices/getInvoiceById"

export default function useInvoiceById(id) {
  const [loading, setLoading] = useState(false)
  const [invoice, setInvoice] = useState([])

  useEffect(
    function () {
      setLoading(true)

      getInvoiceById(id).then((invoice) => {
        setInvoice(invoice)
        setLoading(false)
      })
    },
    [id, setInvoice]
  )

  return { loading, invoice }
}
