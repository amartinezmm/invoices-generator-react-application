import AuthContext from "context/AuthContext"
import { useContext } from "react"

const useAuthContext = () => {
  const user = useContext(AuthContext)
  if (user === undefined) {
    throw new Error("bla bla bla")
  }
  return user
}
