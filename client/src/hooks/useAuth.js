import { useState, useEffect } from "react"

export default function useAuth() {
  const [jwt, setJWT] = useState("")
  const [isAuth, setAuth] = useState(false)
  useEffect(() => {
    setJWT(window.sessionStorage.getItem("jwt"))
    if (jwt) {
      setAuth(true)
    }
  })

  return isAuth
}
