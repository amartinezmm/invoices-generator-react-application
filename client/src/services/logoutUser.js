async function logoutUser() {
  sessionStorage.removeItem("jwt")
  return true
}

export default logoutUser
