import { API_USER_URL } from "services/settings"
export default function registerUser({ username, password, name }) {
  return fetch(`${API_USER_URL}/register`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, name, password }),
  }).then((res) => {
    if (!res.ok) throw new Error("Response is NOT ok")
    return true
  })
}

// export default function register({ username, password }) {
//   const myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve(() => {
//         let test = true
//         if (test) throw new Error("Response is not OK")
//         return true
//       }),
//         reject(() => {
//           return false
//         })
//     }, 300)
//   })
//   myPromise.then((response) => {
//     console.log(response.json())
//     return response
//   })
// }
