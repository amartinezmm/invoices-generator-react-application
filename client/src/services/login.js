import { API_LOGIN_URL } from "services/settings"

export default function login({ username, password }) {
  return fetch(`${API_LOGIN_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  })
    .then((res) => {
      if (!res.ok) throw new Error("Response is NOT ok")
      return res.json()
    })
    .then((res) => {
      const { token, username } = res
      return {
        token,
        username,
      }
    })
}
