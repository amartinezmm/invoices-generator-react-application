import { API_INVOICE_URL } from "services/settings"

export default async function deleteInvoice(id) {
  const jwt = sessionStorage.getItem("jwt")

  return await fetch(`${API_INVOICE_URL}/${id}`, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
      Authorization: "Bearer " + jwt,
    },
  }).then((res) => res.json())
}
