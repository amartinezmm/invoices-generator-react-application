import { API_INVOICE_URL } from "services/settings"

export default function getInvoiceById(id) {
  const jwt = sessionStorage.getItem("jwt")
  const api_invoice_url_id = `${API_INVOICE_URL}/${id}`

  return fetch(api_invoice_url_id, {
    headers: {
      Authorization: "Bearer " + jwt,
    },
  }).then((res) => res.json())
}
