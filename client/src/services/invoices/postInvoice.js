import { API_INVOICE_URL } from "services/settings"

const fromApiResponseToInvoices = (apiResponse) => {
  const { data = [] } = apiResponse
  return data
}

export default function postInvoice({ invoice_id, tax }) {
  return fetch(`${API_INVOICE_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ invoice_id, tax }),
  }).then((res) => res.json())
}
