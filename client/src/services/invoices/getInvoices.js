import { API_INVOICE_URL } from "services/settings"

export default async function getInvoices() {
  const jwt = sessionStorage.getItem("jwt")

  return await fetch(API_INVOICE_URL, {
    headers: {
      Authorization: "Bearer " + jwt,
    },
  }).then((res) => res.json())
}
