export const API_INVOICE_URL = "/api/invoices"
export const API_USER_URL = "/api/users"
export const API_LOGIN_URL = "/api/login"
