import React, { useState } from "react"
import "./App.css"
import { Route, useLocation } from "wouter"

import Detail from "./pages/Detail"
import Home from "./pages/Home"
import Add from "pages/Add"
import Edit from "./pages/Edit"
import Register from "./pages/Register"

import Header from "./components/Header"
import Login from "./components/Login"
import useToken from "./hooks/useToken"

export default function App() {
  const [, navigate] = useLocation()
  const { token, setToken } = useToken()

  if (!token) {
    console.log("in")
    return (
      <>
        <Route component={Register} path="/register/"></Route>
        <Route path="/login">
          {/* {navigate("/login")} */}
          {<Login token={token} setToken={setToken} />}
        </Route>
      </>
    )
  }

  return (
    <div className="App">
      <section className="app-content">
        <Header setToken={setToken} />
        <Route component={Home} path="/"></Route>
        <Route component={Detail} path="/detail/:id"></Route>
        <Route component={Edit} path="/edit/:id"></Route>
        <Route component={Add} path="/add/"></Route>
      </section>
    </div>
  )
}
