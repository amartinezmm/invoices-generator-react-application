import React, { useState } from "react"
import { useLocation } from "wouter"
import useUser from "hooks/useUser"
import { useEffect } from "react"
import "./Login.css"
import loginUser from "services/loginUser"
export default function Login({ token, setToken }) {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [, navigate] = useLocation()
  // const { isLoginLoading, hasLoginError, login, isLogged, jwt } = useUser()
  const isLoginLoading = false
  const hasLoginError = false

  const handleSubmit = async (e) => {
    e.preventDefault()
    const token = await loginUser({
      username,
      password,
    })
    setToken(token.token)
    if (token) {
      navigate("/")
    }
  }

  return (
    <div className="login-wrapper">
      <h1>Please, Log In</h1>
      {isLoginLoading && <strong>Checking credentials...</strong>}
      {!isLoginLoading && (
        <form className="form" onSubmit={handleSubmit}>
          <label>
            <p>Username</p>
            <input
              placeholder="username"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
            />
          </label>

          <label>
            <p>Password</p>
            <input
              type="password"
              placeholder="password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
          </label>
          <div>
            <button type="submit" className="log">
              Login
            </button>
          </div>
        </form>
      )}
      {hasLoginError && <strong>Credentials are invalid</strong>}
    </div>
  )
}
