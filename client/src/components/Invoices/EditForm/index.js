import React, { useEffect, useState } from "react"
import "./InvoiceEditForm.css"
import { getFormattedDate, getMongoDateFormat } from "utils"
import { useLocation } from "wouter"
import { API_INVOICE_URL } from "services/settings"
import Products from "components/Invoices/EditForm/Products"

async function editRecord(id, newValues) {
  const jwt = window.sessionStorage.getItem("jwt")
  const {
    invoice_id,
    date,
    products,
    tax,
    discount,
    total,
    currency,
    payment,
  } = newValues

  await fetch(`${API_INVOICE_URL}/${id}`, {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
      Authorization: "Bearer " + jwt,
    },
    body: JSON.stringify({
      invoice_id: invoice_id,
      date: date,
      products: products,
      tax: tax,
      discount: discount,
      total: total,
      currency: currency,
      payment: payment,
    }),
  }).then((response) => {
    console.log(response)
  })
}

export default function EditForm({ invoice }) {
  const [, navigate] = useLocation()
  const [loading, setLoading] = useState()
  const { id, products } = invoice
  console.log(products)
  const [inputs, setInputs] = useState({})

  useEffect(() => {
    setLoading(true)
    setInputs(invoice)
    setLoading(false)
  }, [invoice])

  const addNewProduct = (event) => {
    event.preventDefault()
    setInputs((inputs) => ({
      ...inputs,
      products: [
        ...inputs.products,
        { name: "", quantity: "", unit_price: "" },
      ],
    }))

    // setInputs((values) => ({
    //   ...values,
    //   product.push(name: "", quantity: "", unit_price: "")},
    // }))
  }

  const handleChange = (event, key) => {
    const name = event.target.name
    const value = event.target.value
    if (name === "date") {
      console.log()
      setInputs((values) => ({ ...values, [name]: getMongoDateFormat(value) }))
    } else if (
      name === "name" ||
      name === "unit_price" ||
      name === "quantity"
    ) {
    } else {
      setInputs((values) => ({ ...values, [name]: value }))
    }
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    editRecord(id, inputs)
    navigate("/")
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Nº de factura
        <input
          type="text"
          name="invoice_id"
          value={inputs.invoice_id || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Fecha
        <input
          type="date"
          name="date"
          value={getFormattedDate(inputs.date) || ""}
          onChange={handleChange}
          required
        />
      </label>
      <h3>Productos</h3>
      <Products products={products} />
      {/* <label>
        Productos
        <input
          type="string"
          name="product"
          value={inputs.product || ""}
          onChange={handleChange}
        />
      </label> */}
      <label>
        Impuestos
        <input
          type="number"
          name="tax"
          value={inputs.tax || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Descuento
        <input
          type="number"
          name="discount"
          value={inputs.discount || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Total
        <input
          type="number"
          name="total"
          value={inputs.total || ""}
          onChange={handleChange}
          required
        />
      </label>
      {/* <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label> */}
      {/* <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label>
      <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label> */}
      <input type="submit" />
    </form>
  )
}

// console.log((values) => ({ ...values, [name]: value }))
// let currentProduct = inputs.product[key]._id
// console.log(currentProduct)

// let updateArr = {
//   ...inputs,
//   product: { ...product[key], [name]: value },
// }

// let newArr = {
//   ...inputs.product[key],
//   ...updateArr.product,
// }
// console.log(updateArr)
// console.log(newArr)

// console.log(setInputs((values, key) => ({ ...inputs, product: "" })))

// let updateArr = inputs.product.map((productObj) =>
//   productObj._id === currentProduct
//     ? { ...productObj, [name]: value }
//     : productObj
// )

// console.log(updateArr)

// setInputs((prevState) => ({
//   ...prevState,
//   product: updateArr,
// }))
