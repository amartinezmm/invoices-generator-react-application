import React, { useEffect, useState } from "react"

export default function Product({ products }) {
  // const handleChange = () => console.log("helo")
  console.log("start here")
  const [inputs, setInputs] = useState([])

  useEffect(() => {
    if (products) {
      setInputs(products)
    }
  }, [])

  const handleChange = (event, id) => {
    const name = event.target.name
    const value = event.target.value

    console.log(name)
    setInputs((values) => ({ ...values, [name]: value }))
  }
  // const handleSubmit = (event) => {
  //   event.preventDefault()
  //   editRecord(id, inputs)
  //   navigate("/")
  // }

  // const { name, quantity, unit_price } = inputs
  console.log("finish  here")
  return products
    ? products.map((product, key) => (
        <div className="product">
          <label>
            Nombre
            <input
              type="text"
              name="name"
              value={product.name || ""}
              onChange={handleChange(product.id)}
              required
            />
          </label>
          <label>
            Cantidad
            <input
              type="number"
              name="quantity"
              value={product.quantity || 0}
              onChange={handleChange(product.id)}
              required
            />
          </label>
          <label>
            Precio
            <input
              type="number"
              name="unit_price"
              value={product.unit_price || 0}
              onChange={(event) => handleChange(event, product.id)}
              required
            />
          </label>
        </div>
      ))
    : ""
}
