import React, { useEffect, useState } from "react"
import "./InvoiceAddForm.css"
import { getFormattedDate, getMongoDateFormat } from "utils"
import { useLocation, Link } from "wouter"
import { API_INVOICE_URL } from "services/settings"

async function addRecord(newValues) {
  const jwt = window.sessionStorage.getItem("jwt")
  const { invoice_id, date, product, tax, discount, total, currency, payment } =
    newValues
  console.log(
    JSON.stringify({
      invoice_id: invoice_id,
      date: date,
      product: product,
      tax: tax,
      discount: discount,
      total: total,
      currency: currency,
      payment: payment,
    })
  )
  await fetch(`${API_INVOICE_URL}/`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Authorization: "Bearer " + jwt,
    },
    body: JSON.stringify({
      invoice_id: invoice_id,
      date: date,
      product: product,
      tax: tax,
      discount: discount,
      total: total,
      currency: currency,
      payment: payment,
    }),
  }).then((response) => {
    console.log(response)
  })
}

export default function AddForm() {
  const [, navigate] = useLocation()
  const [loading, setLoading] = useState()
  // const { id, product } = invoice
  const [product, setProduct] = useState([])
  const [inputs, setInputs] = useState({})

  useEffect(() => {
    setLoading(true)
    setProduct({ name: "" })
  }, [])

  console.log(product)
  if (!loading) {
    console.log(inputs)
  }

  const handleChange = (event, key) => {
    const name = event.target.name
    const value = event.target.value
    if (name === "date") {
      console.log()
      setInputs((values) => ({ ...values, [name]: getMongoDateFormat(value) }))
    } else if (name === "product") {
      setProduct({ ...product, name: value })
      // product[key] = { ...product[key], name: value }

      setInputs((values) => ({
        ...values,
        product: [{ ...product[key], name: value }],
      }))
    } else {
      setInputs((values) => ({ ...values, [name]: value }))
    }
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    addRecord(inputs)
    navigate("/")
  }

  return (
    <form onSubmit={handleSubmit}>
      <strong>
        <Link to="/">Back</Link>
      </strong>

      <label>
        <br></br>
        Nº de factura
        <input
          type="text"
          name="invoice_id"
          value={inputs.invoice_id || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Fecha
        <input
          type="date"
          name="date"
          value={getFormattedDate(inputs.date) || ""}
          onChange={handleChange}
          required
        />
      </label>
      {/* {inputs.product
        ? inputs.product.map((item, key) => (
            <label>
              Productos
              <input
                type="text"
                name="product"
                value={inputs.product[key].name || ""}
                onChange={(e) => handleChange(e, key)}
                required
              />
            </label>
          ))
        : ""} */}

      <label>
        Productos
        <input
          type="string"
          name="product"
          value={product.name || ""}
          onChange={handleChange}
        />
      </label>
      <label>
        Impuestos
        <input
          type="number"
          name="tax"
          value={inputs.tax || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Descuento
        <input
          type="number"
          name="discount"
          value={inputs.discount || ""}
          onChange={handleChange}
          required
        />
      </label>
      <label>
        Total
        <input
          type="number"
          name="total"
          value={inputs.total || ""}
          onChange={handleChange}
          required
        />
      </label>
      {/* <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label> */}
      {/* <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label>
      <label>
        Enter your age:
        <input
          type="date"
          name="date"
          value={getFormattedDate(date) || ""}
          onChange={handleChange}
        />
      </label> */}
      <input type="submit" />
    </form>
  )
}
