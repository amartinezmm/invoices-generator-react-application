import React, { useState } from "react"
import "./InvoiceDetail.css"
import { getFormattedDate } from "utils"

export default function InvoiceDetail({ invoice }) {
  const { id, invoice_id, tax, product, date, currency, payment, total } =
    invoice
  const [loading, setLoading] = useState()

  return invoice ? (
    <div key={id} className="invoice-box">
      <table cellPadding="0" cellSpacing="0">
        <tr className="top">
          <td colSpan="2">
            <table>
              <tr>
                <td className="title">
                  <h1>Logo compañia</h1>
                </td>

                <td>
                  Nº factura: {invoice_id}
                  <br />
                  Fecha: {getFormattedDate(date)}
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr className="information">
          <td colSpan="2">
            <table>
              <tr>
                <td>
                  Nombre cliente
                  <br />
                  Dirección
                  <br />
                  Estado
                </td>

                <td>
                  Nombre empresa
                  <br />
                  Alex Martinez
                  <br />
                  alexmartinez@gmail.com
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr className="heading-method-payment">
          <td colSpan="2">Método de pago</td>
          {/* TODO */}
        </tr>
        {payment
          ? payment.map((payment) => (
              <tr className="details">
                <td>{payment.method}</td>

                <td>{payment.information}</td>
              </tr>
            ))
          : ""}

        <tr className="heading">
          <td>Productos</td>

          <td>Precio</td>
        </tr>

        {product
          ? product.map((item) => (
              <tr className="item">
                <td>{item.name}</td>
                <td>
                  {item.unit_price} {currency}
                </td>
              </tr>
            ))
          : ""}

        <tr className="total">
          <td></td>

          <td>{total}</td>
        </tr>
      </table>
    </div>
  ) : (
    "<h1>Loading...</h1>"
  )
}
