import { Link } from "wouter"
import useInvoices from "hooks/useInvoices"
import "./InvoicesRecords.css"
import { getFormattedDate } from "utils"

import deleteInvoice from "services/invoices/deleteInvoice"

function Invoice({ invoice, invoicesList, updateInvoices }) {
  const { id, invoice_id, date, products, total, currency, user } = invoice

  const deleteRecord = (id) => {
    deleteInvoice(id)
    const updateList = invoicesList.filter((el) => el.id !== id)
    updateInvoices(updateList)
  }

  return (
    <>
      <tr>
        <td>{invoice_id}</td>
        <td>{getFormattedDate(date)}</td>
        <td>{products.length}</td>
        <td>
          {total} {currency}
        </td>
        <td>{user.username}</td>
        <td>
          <Link className="btn btn-link" to={`/edit/${id}`}>
            ✏️
          </Link>
          <Link to={`/detail/${id}`}>🔎</Link>
          <button
            className=""
            onClick={() => {
              deleteRecord(id)
            }}
          >
            🗑️
          </button>
        </td>
      </tr>
    </>
  )
}

export default function InvoicesRecords() {
  const { loading, invoices, setInvoices } = useInvoices()

  // This method will map out the records on the table
  function invoiceList() {
    return invoices
      ? invoices.map((invoice) => (
          <Invoice
            invoice={invoice}
            key={invoice.id}
            invoicesList={invoices}
            updateInvoices={setInvoices}
          />
        ))
      : ""
  }

  return (
    <div>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          <h3>Lista de facturas</h3>
          <table className="table table-striped" style={{ marginTop: 20 }}>
            <thead>
              <tr>
                <th>Nº de factura</th>
                <th>Fecha</th>
                <th>Bultos</th>
                <th>Total</th>
                <th>Creado por</th>
              </tr>
            </thead>
            <tbody>{invoiceList()}</tbody>
          </table>
        </>
      )}
    </div>
  )
}
