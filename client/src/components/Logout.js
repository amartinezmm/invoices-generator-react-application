import { useLocation, Link } from "wouter"
import logoutUser from "services/logoutUser"

export default function Logout({ setToken }) {
  const [, navigate] = useLocation()

  const handleClick = async (e) => {
    e.preventDefault()
    const token = await logoutUser()
    setToken(null)
    if (!token) {
      navigate("/login")
    }
  }

  return (
    <Link to="/login" onClick={handleClick}>
      Logout
    </Link>
  )
}
