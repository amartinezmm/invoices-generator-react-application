import React from "react"
import { useRoute, Link, useLocation } from "wouter"
import Logout from "components/Logout"

import "./Header.css"

export default function Header({ setToken }) {
  const [, navigate] = useLocation()
  // const { isLogged, logout } = useUser()
  const [match] = useRoute("/login")

  // const renderLoginButtons = ({ isLogged }) => {
  //   return isLogged ? (
  //     <Link to="/login" onClick={handleClick}>
  //       Logout
  //     </Link>
  //   ) : (
  //     <>
  //       <Link to="/login">Login</Link>
  //       <Link to="/register">Register</Link>
  //     </>
  //   )
  // }

  const content = match ? null : (
    <div>
      <Logout />
      <Link to="/add/">Add</Link>
    </div>
  )

  return <header className="gf-header">{content}</header>
}
