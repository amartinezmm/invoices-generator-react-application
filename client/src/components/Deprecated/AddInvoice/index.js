import React, { useState } from "react"
import register from "services/register"
import postInvoice from "services/invoices/postInvoice"
import { Formik, Form, Field, ErrorMessage } from "formik"

const validateFields = (values) => {
  const errors = {}

  if (!values.invoice_id) {
    errors.invoice_id = "Required invoice id"
  }

  if (!values.tax) {
    errors.tax = "Required tax"
  } else if (values.tax.length < 3) {
    errors.tax = "Length must be greater than 3"
  }

  return errors
}

const initialValues = {
  invoice_id: "",
  tax: "",
}

export default function AddInvoice() {
  const [created, setCreated] = useState(false)

  if (created) {
    return <h4>Congratulations ✅! You've added a new invoice!</h4>
  }

  return (
    <>
      {/* <Formik
        initialValues={initialValues}
        validate={validateFields}
        onSubmit={(values, { setFieldError }) => {
          return register(values)
            .then(() => {
              setRegistered(true)
            })
            .catch(() => {
              setFieldError("username", "This username is not valid")
            })
        }}
      > */}
      <Formik
        initialValues={initialValues}
        validate={validateFields}
        onSubmit={(values, { setFieldError }) => {
          return postInvoice(values)
            .then(() => {
              setCreated(true)
              console.log("Good")
            })
            .catch(() => {
              alert("Error")
              setFieldError("username", "This username is not valid")
            })
        }}
      >
        {({ errors, isSubmitting }) => (
          <Form className="form">
            <Field
              className={errors.invoice_id ? "error" : ""}
              name="invoice_id"
              placeholder="Put here the invoice_id"
            />
            <ErrorMessage
              className="form-error"
              name="invoice_id"
              component="small"
            />

            <Field
              className={errors.tax ? "error" : ""}
              name="tax"
              placeholder="Put the tax value here"
              type="tax"
            />
            <ErrorMessage className="form-error" name="tax" component="small" />

            <button className="btn" disabled={isSubmitting}>
              Add invoice
            </button>
          </Form>
        )}
      </Formik>
    </>
  )
}
