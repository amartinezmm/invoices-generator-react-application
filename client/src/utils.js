export function getFormattedDate(date_export) {
  const date = new Date(date_export)

  var year = date.getFullYear()

  var month = (1 + date.getMonth()).toString()
  month = month.length > 1 ? month : "0" + month

  var day = date.getDate().toString()
  day = day.length > 1 ? day : "0" + day

  return year + "-" + month + "-" + day
}

export function getMongoDateFormat(date) {
  console.log("here")
  console.log(date)
  let test_date = new Date(date)
  console.log(test_date.toISOString())
  return test_date.toISOString()
}
