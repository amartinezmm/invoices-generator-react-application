import useInvoiceById from "hooks/useInvoiceById"
import EditForm from "components/Invoices/EditForm"
export default function Edit({ params }) {
  const { loading, invoice } = useInvoiceById(params.id)

  return (
    <div>
      {loading ? (
        <h3>Loading...</h3>
      ) : (
        <EditForm key={invoice.id} invoice={invoice} />
      )}
    </div>
  )
}
