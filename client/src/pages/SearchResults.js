import { useEffect, useState } from "react";
import Spinner from "../components/Spinner";
import ListOfGifs from "../components/ListOfGifs";
import getGifs from "../services/getGifs";
import useGifs from "../hooks/useGifs";

export default function SearchResults({ params }) {
  const { keyword } = params;
  const { loading, gifs } = useGifs({ keyword });
  return <div>{loading ? <Spinner /> : <ListOfGifs gifs={gifs} />}</div>;

  // return <>{loading ? <Spinner /> : <ListOfGifs gifs={gifs} />}</>;
}
