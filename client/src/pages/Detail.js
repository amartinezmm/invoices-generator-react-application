import useInvoiceById from "hooks/useInvoiceById"
import InvoiceDetail from "components/Invoices/InvoiceDetail"
export default function Detail({ params }) {
  console.log(params.id)
  const { loading, invoice } = useInvoiceById(params.id)

  return (
    <div>
      {loading ? (
        <h3>Loading...</h3>
      ) : (
        <InvoiceDetail key={invoice.id} invoice={invoice} />
      )}
    </div>
  )
}
