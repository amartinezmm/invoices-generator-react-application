import React, { useState } from "react"
import { Link, useLocation } from "wouter"
import Records from "components/Invoices/Records"

export default function Home() {
  const [path, pushLocation] = useLocation()

  return (
    <>
      <Records />
    </>
  )
}
